package main

import (
	"net/http"
)

var (
	db *storage
)

func init() {
	db, _ = newStorage()
}

func main() {

	http.HandleFunc("/store", store)
	http.HandleFunc("/flush", flush)
	http.HandleFunc("/lookup", lookup)
	http.HandleFunc("/all", retrieveAll)
	http.HandleFunc("/retrieve", retrieve)

	if err := http.ListenAndServe(":8000", nil); err != nil {
		panic(err)
	}
}
