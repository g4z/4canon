
# Audit system prototype

> Summary

- Implemented as a JSON speaking HTTP API on port 8000
- Recieves JSON format event records and stores them in a map
- A mutex is used to syncronise access to the shared map
- Generates a UUID for each event
- Retrieve event records by UUID or arbitrary context data (eg. customer_id)

> Todo

- Validate input
- Authentication and TLS
- Error handling cases

> Running

```bash
# requires: make, docker, docker-compose
make up

# run benchmark
make bench

# alternatively, start without docker
go run *.go
```

> Endpoints

|URL | Method | Purpose | Note
|-|-|-|-
|http://localhost:8000/store|POST|store event|
|http://localhost:8000/retrieve|GET|retrieve event by ID|
|http://localhost:8000/lookup|GET|retrieve event by context|
|http://localhost:8000/all|GET|retrieve all events|*debug*
|http://localhost:8000/flush|GET|remove all events|*debug*


> By Endpoint

### Store (POST)

- Receives JSON format audit records and stores them


```bash
$ curl -is -H "Content-Type: application/json" \
> --data-binary "@benchmark/example-small.json" \
> localhost:8000/store
HTTP/1.1 200 OK
Content-Type: application/json
Date: Tue, 28 May 2019 22:26:13 GMT
Content-Length: 60

{"message":"ok","id":"a072c25a-3a9a-4b66-954a-336f27d27d95"}
```

### Retrieve (GET)

- Recieves an event ID and returns the event data

```bash
$curl -is localhost:8000/retrieve?id=a072c25a-3a9a-4b66-954a-336f27d27d95
HTTP/1.1 200 OK
Content-Type: application/json
Date: Tue, 28 May 2019 22:29:35 GMT
Content-Length: 321

{"id":"a072c25a-3a9a-4b66-954a-336f27d27d95","timest ....
```

### Lookup (GET)

- Performs a lookup on the context data

```bash
$ curl -is localhost:8000/lookup?cid=12345
HTTP/1.1 200 OK
Content-Type: application/json
Date: Tue, 28 May 2019 22:34:48 GMT
Content-Length: 362

{"a072c25a-3a9a-4b66-954a-336f27d27d95":{"id":"a072c ....
```

### RetrieveAll (GET)

- Returns all event records (for debugging)

```bash
$ curl -is localhost:8000/all
HTTP/1.1 200 OK
Content-Type: application/json
Date: Tue, 28 May 2019 22:36:12 GMT
Content-Length: 362

{"a072c25a-3a9a-4b66-954a-336f27d27d95":{"id":"a072c ....
```

### Flush (GET)

- Removes all event records (for debugging)

```bash
$ curl -is localhost:8000/flush
HTTP/1.1 200 OK
Content-Type: application/json
Date: Tue, 28 May 2019 22:37:15 GMT
Content-Length: 16

{"message":"ok"}
```
> Benchmarks

- some unscientific benchmarks from my laptop

```
payload => example-small.json (len:377) | concurrency => 5 | rate =>     12193.25 [#/sec] (mean)
payload => example-small.json (len:377) | concurrency => 10 | rate =>     11974.87 [#/sec] (mean)
payload => example-small.json (len:377) | concurrency => 25 | rate =>     12259.98 [#/sec] (mean)
payload => example-small.json (len:377) | concurrency => 50 | rate =>     11614.24 [#/sec] (mean)
payload => example-small.json (len:377) | concurrency => 100 | rate =>     11325.54 [#/sec] (mean)
payload => example-small.json (len:377) | concurrency => 250 | rate =>     11186.76 [#/sec] (mean)
payload => example-small.json (len:377) | concurrency => 500 | rate =>     10965.49 [#/sec] (mean)
payload => example-small.json (len:377) | concurrency => 750 | rate =>     10807.87 [#/sec] (mean)
payload => example-small.json (len:377) | concurrency => 1000 | rate =>     10323.52 [#/sec] (mean)

payload => example-medium.json (len:9720) | concurrency => 5 | rate =>     5808.89 [#/sec] (mean)
payload => example-medium.json (len:9720) | concurrency => 10 | rate =>     6057.58 [#/sec] (mean)
payload => example-medium.json (len:9720) | concurrency => 25 | rate =>     6162.48 [#/sec] (mean)
payload => example-medium.json (len:9720) | concurrency => 50 | rate =>     6183.73 [#/sec] (mean)
payload => example-medium.json (len:9720) | concurrency => 100 | rate =>     5801.96 [#/sec] (mean)
payload => example-medium.json (len:9720) | concurrency => 250 | rate =>     5577.72 [#/sec] (mean)
payload => example-medium.json (len:9720) | concurrency => 500 | rate =>     5816.67 [#/sec] (mean)
payload => example-medium.json (len:9720) | concurrency => 750 | rate =>     5545.71 [#/sec] (mean)
payload => example-medium.json (len:9720) | concurrency => 1000 | rate =>     5700.42 [#/sec] (mean)
```
