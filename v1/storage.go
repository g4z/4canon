package main

import (
	"sync"
)

type istorage interface {
	add(*event)
	get(string) *event
	lookup(string, string) []*event
	all() []*event
	flush()
}

func newStorage() (*storage, error) {
	s := new(storage)
	s.events = make(map[string]*event)
	return s, nil
}

type storage struct {
	m      sync.Mutex
	events map[string]*event
}

func (s *storage) add(e *event) {
	s.m.Lock()
	s.events[e.ID] = e
	s.m.Unlock()
}

func (s *storage) get(key string) *event {
	s.m.Lock()
	defer s.m.Unlock()
	for id := range s.events {
		if id == key {
			return s.events[key]
		}
	}
	return nil
}

func (s *storage) lookup(key string, val string) []*event {
	s.m.Lock()
	defer s.m.Unlock()
	events := []*event{}
	for id := range s.events {
		if s.events[id].Context.(map[string]interface{})[key].(string) == val {
			events = append(events, s.events[id])
		}
	}
	return events
}

func (s *storage) all() []*event {
	s.m.Lock()
	defer s.m.Unlock()
	events := []*event{}
	for id := range s.events {
		events = append(events, s.events[id])
	}
	return events
}

func (s *storage) flush() {
	s.m.Lock()
	defer s.m.Unlock()
	s.events = map[string]*event{}
}
