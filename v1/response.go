package main

import (
	"fmt"
	"io"
	"net/http"
)

// Write an http error response
func failure(w http.ResponseWriter, err error, status int) {

	w.Header().Add("Content-Type", "application/json")

	w.WriteHeader(status)

	response := fmt.Sprintf(`{"message":"%s"}`, err.Error())

	io.WriteString(w, response)
}

// Write an http 200 status response
func success(w http.ResponseWriter, response string) {

	w.Header().Add("Content-Type", "application/json")

	io.WriteString(w, response)
}
