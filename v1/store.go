package main

import (
	"crypto/rand"
	"fmt"
	"io/ioutil"
	"net/http"
)

func store(w http.ResponseWriter, r *http.Request) {

	var e *event
	var err error
	var body []byte

	if body, err = ioutil.ReadAll(r.Body); err != nil {
		failure(w, err, http.StatusBadRequest)
		return
	}

	if e, err = eventFromJSON(body); err != nil {
		failure(w, err, http.StatusBadRequest)
		return
	}

	if err := e.validate(); err != nil {
		failure(w, err, http.StatusBadRequest)
		return
	}

	e.ID = generateUUID()

	db.add(e)

	response := fmt.Sprintf(`{"message":"ok","id":"%s"}`, e.ID)

	success(w, response)
}

func generateUUID() string {

	buf := make([]byte, 16)

	if _, err := rand.Read(buf); err != nil {
		panic(err) // hello
	}

	return fmt.Sprintf(
		"%x-%x-%x-%x-%x",
		buf[:4],
		buf[4:6],
		buf[6:8],
		buf[8:10],
		buf[10:],
	)

	// or use github.com/google/uuid
	// return uuid.New().String()
}
