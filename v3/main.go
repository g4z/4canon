package main

import (
	"flag"
)

var (
	db *storage
)

func init() {
	var err error
	if db, err = newStorage(); err != nil {
		panic(err.Error())
	}
}

func main() {

	worker := flag.Bool("worker", false, "enable queue worker mode")

	flag.Parse()

	var err error

	if *worker {
		err = startWorker()
	} else {
		err = startHTTP()
	}

	if err != nil {
		panic(err.Error())
	}
}
