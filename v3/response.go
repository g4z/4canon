package main

import (
	"fmt"
	"io"
	"net/http"
)

// Write an http error response to the requestor
func failure(w http.ResponseWriter, err error, status int) {

	w.Header().Add("Content-Type", "application/json")

	w.WriteHeader(status)

	response := fmt.Sprintf(`{"message":"%s"}`, err.Error())

	io.WriteString(w, response)
}

func success(w http.ResponseWriter, response string) {

	w.Header().Add("Content-Type", "application/json")

	io.WriteString(w, response)
}
