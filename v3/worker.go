package main

import (
	"encoding/json"
	"fmt"
)

func startWorker() error {

	var payload []byte
	var err error

	e := &event{}
	events := []*event{}

	ch := db.queue.Subscribe("default").Channel()
	defer db.queue.Subscribe("default").Close()

	for {

		payload = []byte((<-ch).Payload)
		if e, err = eventFromJSON(payload); err != nil {
			return err
		}

		if err := e.validate(); err != nil {
			return err
		}

		events = append(events, e)

		// Buffer 250 records before writing to database
		if len(events) >= 250 {

			sql := `INSERT INTO events
(id,timestamp,origin,channel,type,subtype,tags,message,context) VALUES `

			for _, _e := range events {

				// TODO error checking
				tags, _ := json.Marshal(e.Tags)
				context, _ := json.Marshal(e.Context)

				sql += fmt.Sprintf(
					"('%s','%s','%s','%s','%s','%s','%s','%s','%s'),",
					_e.ID,
					_e.Timestamp,
					_e.Origin,
					_e.Channel,
					_e.Type,
					_e.Subtype,
					tags,
					_e.Message,
					context,
				)
			}

			// remove trailing comma
			sql = sql[0 : len(sql)-1]

			// execute statement
			if _, err = db.database.Exec(sql); err != nil {
				return err
			}

			// flush in-memory events
			events = []*event{}
		}
	}

}
