
# Audit system prototype

> Version using a pubsub channel and SQL database

- Forwards events to a Redis pubsub channel
- A worker process retrieves events from the pubsub channel and loads them into an SQL database

See [this README](../v1/README.md) for more information.
