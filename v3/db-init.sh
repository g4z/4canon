#!/bin/bash

sleep 3

CMD="/cockroach/cockroach.sh sql --host db --insecure"

# $CMD -e "DROP DATABASE audit_log;"
$CMD -e "CREATE DATABASE audit_log;"
$CMD -d audit_log -e 'CREATE TABLE "events" (
    id UUID PRIMARY KEY,
    timestamp TIMESTAMPTZ,
    origin STRING(255),
    channel STRING(255),
    type STRING(255),
    subtype STRING(255),
    tags JSONB,
    message STRING(255),
    context JSONB
);
'
