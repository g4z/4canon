package main

import "net/http"

func startHTTP() error {

	http.HandleFunc("/store", store)
	http.HandleFunc("/flush", flush)
	http.HandleFunc("/lookup", lookup)
	http.HandleFunc("/all", retrieveAll)
	http.HandleFunc("/retrieve", retrieve)

	if err := http.ListenAndServe(":8000", nil); err != nil {
		return err
	}

	return nil
}
