package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"os"

	"github.com/go-redis/redis"

	_ "github.com/lib/pq"
)

type istorage interface {
	add(*event)
	get(string) *event
	lookup(string, string) []*event
	all() []*event
	flush()
}

func newStorage() (*storage, error) {

	queueHost := os.Getenv("QUEUE_HOST")
	queuePort := os.Getenv("QUEUE_PORT")

	dbHost := os.Getenv("DB_HOST")
	dbPort := os.Getenv("DB_PORT")
	dbUser := os.Getenv("DB_USER")
	dbSchema := os.Getenv("DB_SCHEMA")

	s := new(storage)

	s.queue = redis.NewClient(
		&redis.Options{
			Addr: queueHost + ":" + queuePort,
		},
	)

	var err error
	if s.database, err = sql.Open(
		"postgres",
		fmt.Sprintf(
			"postgresql://%s@%s:%s/%s?sslmode=disable",
			dbUser,
			dbHost,
			dbPort,
			dbSchema,
		)); err != nil {
		return s, err
	}

	return s, nil
}

type storage struct {
	queue    *redis.Client
	database *sql.DB
}

func (s *storage) add(e *event) {

	var err error
	var buf []byte

	if buf, err = json.Marshal(e); err != nil {
		return // TODO handle err
	}

	if err = s.queue.Publish(e.Channel, buf).Err(); err != nil {
		return // TODO handle err
	}
}

func (s *storage) get(key string) *event {

	var q string
	var err error
	var row *sql.Row
	var tags []string
	var context map[string]interface{}
	var id, timestamp, origin, channel, _type, subtype, message string

	q = `select * from events where id=$1`
	if row = s.database.QueryRow(q, key); row == nil {
		return nil // TODO handle err
	}

	if err = row.Scan(
		&id,
		&timestamp,
		&origin,
		&channel,
		&_type,
		&subtype,
		&message,
		&tags,
		&context,
	); err != nil {
		return nil // TODO handle err
	}

	e := event{}

	e.ID = id
	e.Timestamp = timestamp
	e.Origin = origin
	e.Channel = channel
	e.Type = _type
	e.Subtype = subtype
	e.Message = message
	e.Tags = tags
	e.Context = context

	return &e
}

func (s *storage) lookup(key string, val string) []*event {
	// TODO implement the lookup using CockroachDB BJSON
	return nil
}

func (s *storage) all() []*event {

	var err error
	var rows *sql.Rows

	e := event{}
	events := []*event{}

	if rows, err = s.database.Query(`select * from events`); err != nil {
		return nil // fail silently for now
	}

	for rows.Next() {

		var tags []string
		var context map[string]interface{}
		var id, timestamp, origin, channel, _type, subtype, message string

		if err = rows.Scan(
			&id,
			&timestamp,
			&origin,
			&channel,
			&_type,
			&subtype,
			&message,
			&tags,
			&context,
		); err != nil {
			return nil
		}

		e.ID = id
		e.Timestamp = timestamp
		e.Origin = origin
		e.Channel = channel
		e.Type = _type
		e.Subtype = subtype
		e.Message = message
		e.Tags = tags
		e.Context = context

		events = append(events, &e)
	}

	return events
}

func (s *storage) flush() {
	_, _ = s.database.Exec(`truncate events`)
}
