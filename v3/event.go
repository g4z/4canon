package main

import (
	"encoding/json"
)

func eventFromJSON(buf []byte) (*event, error) {
	e := new(event)
	if err := json.Unmarshal(buf, e); err != nil {
		return e, err
	}
	return e, nil
}

type event struct {

	// Auto-generated UUID for this event
	ID string `json:"id"`

	// Original event timestamp (from source)
	Timestamp string `json:"timestamp"`

	// Origin identifier (eg. `web3.somehost.net`)
	Origin string `json:"origin"`

	// Top level grouping for filtering
	Channel string `json:"channel"`

	// Type of event (eg. `customer` event)
	Type string `json:"type"`

	// Subtype type (eg. `update`)
	Subtype string `json:"subtype"`

	// Textual message
	Message string `json:"message"`

	// Arbitrary tags
	Tags []string `json:"tags"`

	// Context data
	Context interface{} `json:"context"`
}

func (e *event) validate() error {
	// TODO validate event
	return nil
}
