#!/usr/bin/env sh

timelimit=5
concurrencies="5 10 25 50 100 250 500 750 1000"
payloads="example-small.json example-medium.json"

for payload in $payloads
do
    len=$(cat /tmp/$payload | wc -c | tr -d "\n")

    for concurrency in $concurrencies
    do
        echo -n "payload => $payload (len:$len) | concurrency => $concurrency | rps => "

        ab -p /tmp/$payload \
            -m POST \
            -T application/json \
            -c $concurrency \
            -t $timelimit \
            http://app:8000/store \
            2>/dev/null \
            | grep 'Requests per second:' \
            | sed 's/Requests per second://g'

        # remove previous events
        ab -m POST -T application/json \
            -c 1 \
            -n 1 \
            http://app:8000/flush \
            >/dev/null

    done
done