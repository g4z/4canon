package main

type istorage interface {
	add(*event)
	get(string) *event
	lookup(string, string) []*event
	all() []*event
	flush()
}

func newStorage() (*storage, error) {
	s := new(storage)
	s.events = make(map[string]*event)
	return s, nil
}

type storage struct {
	events map[string]*event
}

func (s *storage) add(e *event) {
	s.events[e.ID] = e
}

func (s *storage) get(key string) *event {
	for id := range s.events {
		if id == key {
			return s.events[key]
		}
	}
	return nil
}

func (s *storage) lookup(key string, val string) []*event {
	events := []*event{}
	for id := range s.events {
		if s.events[id].Context.(map[string]interface{})[key].(string) == val {
			events = append(events, s.events[id])
		}
	}
	return events
}

func (s *storage) all() []*event {
	events := []*event{}
	for id := range s.events {
		events = append(events, s.events[id])
	}
	return events
}

func (s *storage) flush() {
	s.events = map[string]*event{}
}
