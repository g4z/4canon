package main

import (
	"net/http"
)

// Endpoint to remove all logs from storage (for debugging)
func flush(w http.ResponseWriter, r *http.Request) {

	flushRequest <- &struct{}{}

	success(w, `{"message":"ok"}`)
}
