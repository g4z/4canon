package main

import (
	"net/http"
)

var (
	db                  *storage
	storeRequest        chan *event
	storeResponse       chan *struct{}
	retrieveRequest     chan *string
	retrieveResponse    chan *event
	retrieveAllRequest  chan *struct{}
	retrieveAllResponse chan []*event
	flushRequest        chan *struct{}
	flushResponse       chan *struct{}
	lookupRequest       chan *string
	lookupResponse      chan []*event
)

func init() {

	db, _ = newStorage()

	storeRequest = make(chan *event)
	retrieveRequest = make(chan *string)
	retrieveResponse = make(chan *event)
	retrieveAllRequest = make(chan *struct{})
	retrieveAllResponse = make(chan []*event)
	flushRequest = make(chan *struct{})
	flushResponse = make(chan *struct{})
	lookupRequest = make(chan *string)
	lookupResponse = make(chan []*event)
}

func main() {

	http.HandleFunc("/store", store)
	http.HandleFunc("/flush", flush)
	http.HandleFunc("/lookup", lookup)
	http.HandleFunc("/all", retrieveAll)
	http.HandleFunc("/retrieve", retrieve)

	go func() {
		if err := http.ListenAndServe(":8000", nil); err != nil {
			panic(err)
		}
	}()

	for {
		select {
		case event := <-storeRequest:
			db.add(event)
		case id := <-retrieveRequest:
			retrieveResponse <- db.get(*id)
		case <-retrieveAllRequest:
			retrieveAllResponse <- db.all()
		case <-flushRequest:
			db.flush()
		case cid := <-lookupRequest:
			lookupResponse <- db.lookup("customer_id", *cid)
		}
	}
}
