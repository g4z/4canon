package main

import (
	"encoding/json"
	"errors"
	"net/http"
)

// Endpoint to return an event from ID
func retrieve(w http.ResponseWriter, r *http.Request) {

	var e *event
	var id string
	var err error
	var response []byte

	if id = r.URL.Query().Get("id"); id == "" {
		failure(w, errors.New("Bad Request"), http.StatusBadRequest)
		return
	}

	retrieveRequest <- &id
	e = <-retrieveResponse

	if e == nil {
		failure(w, errors.New("Not Found"), http.StatusNotFound)
		return
	}

	if response, err = json.Marshal(e); err != nil {
		failure(w, err, http.StatusInternalServerError)
		return
	}

	success(w, string(response))
}

// Endpoint to return all logs (for debugging)
func retrieveAll(w http.ResponseWriter, r *http.Request) {

	var err error
	var response []byte
	var events []*event

	retrieveAllRequest <- &struct{}{}
	events = <-retrieveAllResponse

	if events == nil {
		success(w, string("[]"))
		return
	}

	if response, err = json.Marshal(events); err != nil {
		failure(w, err, http.StatusInternalServerError)
		return
	}

	success(w, string(response))
}
