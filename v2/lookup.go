package main

import (
	"encoding/json"
	"errors"
	"net/http"
)

func lookup(w http.ResponseWriter, r *http.Request) {

	var err error
	var cid string
	var response []byte
	var events []*event

	if cid = r.URL.Query().Get("cid"); cid == "" {
		failure(w, errors.New("Bad Request"), http.StatusBadRequest)
		return
	}

	lookupRequest <- &cid
	events = <-lookupResponse

	if response, err = json.Marshal(events); err != nil {
		failure(w, err, http.StatusInternalServerError)
		return
	}

	success(w, string(response))
}
