
# Audit system prototype

> http://35.157.245.132:8000

- The source code for the live version is [here](./v1/)

### Endpoints

|URL | Method | Purpose | Note
|-|-|-|-
|http://35.157.245.132:8000/store|POST|store event|
|http://35.157.245.132:8000/retrieve|GET|retrieve event by ID|
|http://35.157.245.132:8000/lookup|GET|retrieve event by context|
|http://35.157.245.132:8000/all|GET|retrieve all events|*debug*
|http://35.157.245.132:8000/flush|GET|remove all events|*debug*

## Schema

This is the schema for the incoming JSON event records:

```
// Original event timestamp
Timestamp string

// Origin of this event (eg. hostname)
Origin string

// Top level grouping (eg. "staging")
Channel string

// Event type (eg. customer)
Type string

// Subtype type (eg. update)
Subtype string

// Tags for lookup
Tags []string

// Text message
Message string

// Context data
Context interface{}
```

The `tags` property stores string value tags, could be useful.

The `context` property stores arbitrary data and is intended for storage of event-type specific data.

- See [example-small.json](benchmark/example-small.json) for an example schema

## Sample implementation code

|folder|description
|-|-
|[v1](v1)|example implementation using a map and mutex
|[v2](v2)|example implementation using channels
|[v3](v3)|example implementation using redis + database

# Code layout

```
.
├── benchmark
│   ├── benchmark.sh
│   ├── Dockerfile
│   ├── example-medium.json
│   └── example-small.json
├── README.md
├── v1
│   ├── docker-compose.yml
│   ├── Dockerfile
│   ├── event.go
│   ├── flush.go
│   ├── lookup.go
│   ├── main.go
│   ├── Makefile
│   ├── README.md
│   ├── response.go
│   ├── retrieve.go
│   ├── storage.go
│   └── store.go
├── v2
│   ├── docker-compose.yml
│   ├── Dockerfile
│   ├── event.go
│   ├── flush.go
│   ├── lookup.go
│   ├── main.go
│   ├── Makefile
│   ├── README.md
│   ├── response.go
│   ├── retrieve.go
│   ├── storage.go
│   └── store.go
└── v3
    ├── db-init.sh
    ├── docker-compose.yml
    ├── Dockerfile
    ├── event.go
    ├── flush.go
    ├── http.go
    ├── lookup.go
    ├── main.go
    ├── Makefile
    ├── README.md
    ├── response.go
    ├── retrieve.go
    ├── storage.go
    ├── store.go
    └── worker.go

4 directories, 44 files
```

- See this v1 [README](v1/README.md) for more information
